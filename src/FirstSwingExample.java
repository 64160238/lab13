import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("First JFrame"); // กำหนดชื่อ
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40); //ปุ่ม Click จากด้านซ้าย130 จากด้านบน100 กว้าง100 ยาว40
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400, 500); // กำหนดขนาด
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ออกโปรแกรม
        frame.setVisible(true);
    }
}
