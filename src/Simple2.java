import javax.swing.*;

class MyFrame extends JFrame{
    JButton button;
    public MyFrame() {
        super("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40); //ปุ่ม Click จากด้านซ้าย130 จากด้านบน100 กว้าง100 ยาว40
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500); // กำหนดขนาด
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ออกโปรแกรม
        this.setVisible(true);
    }
}
public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
    }
}
